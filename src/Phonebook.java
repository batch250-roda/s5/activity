import java.util.ArrayList;

public class Phonebook {
    private ArrayList<Contact> contacts = new ArrayList<Contact>();

    private Contact contact;

    public Phonebook() {}


    public Phonebook(ArrayList<Contact> contacts){
        this.contacts = contacts;
    }

    public ArrayList<Contact> getContacts(){
        return this.contacts;
    }

    public void setContacts(ArrayList<Contact>  contacts){
        this.contacts = contacts;
    }

    public void printInfo(String name){
        if(contacts != null){
            contacts.forEach(contact ->{
                if(contact.getName() == name){
                     if (contact.getAddress() == "" && contact.getContactNumber() == "") {
                         System.out.println(name);
                         System.out.println("----------------------");
                         System.out.println(name + " has not registered a number");
                         System.out.println(name + " has not registered an address");
                    } else if (contact.getContactNumber() == "") {
                        System.out.println(name);
                        System.out.println("----------------------");
                        System.out.println(name + " has not registered a number");
                        System.out.println(name + " has the following registered address: " + contact.getAddress());
                    } else if (contact.getAddress() == "") {
                         System.out.println(name);
                         System.out.println("----------------------");
                         System.out.println(name + " has the following registered number: " + contact.getContactNumber());
                         System.out.println(name + " has not registered an address");
                     } else{
                        System.out.println(name);
                        System.out.println("----------------------");
                        System.out.println(name + " has the following registered number: " + contact.getContactNumber());
                        System.out.println(name + " has the following registered address: " + contact.getAddress());
                    }
                }
            });
        }


    }


}
