import java.util.ArrayList;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Phonebook phonebook = new Phonebook();

        Contact contact1 = new Contact("Jayson Roda", "09123456789", "Tinago Baybay City");
        Contact contact2 = new Contact("Nerissa Gumba", "09987654321", "Gakat Baybay City");

        Contact contact3 = new Contact("Test", "", "");

        ArrayList<Contact> contacts = new ArrayList<Contact>();
        contacts.add(contact1);
        contacts.add(contact2);
        contacts.add(contact3);
        phonebook.setContacts(contacts);



        phonebook.getContacts().forEach(contact -> {
            if(contact != null){
                System.out.println(contact.getName());
                System.out.println("----------");
                System.out.println("Contact Number: " + contact.getContactNumber());
                System.out.println("Address: " + contact.getAddress());
                System.out.println();
            }

        });

        phonebook.printInfo(contact3.getName());
    }
}